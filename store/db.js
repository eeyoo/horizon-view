var Sequelize = require('sequelize');
var sequelize = new Sequelize('mysql://root:root@127.0.0.1:3306/power');


var User = sequelize.define('user', {
	name: { type: Sequelize.STRING },
	age: { type: Sequelize.INTEGER },
	sex: { type: Sequelize.INTEGER },
	birth: { type: Sequelize.STRING },
	address: { type: Sequelize.STRING },
	phone: { type: Sequelize.STRING },
	state: { type: Sequelize.INTEGER }
}, {freezeTableName: true});

sequelize.authenticate().then(function() {
	console.log('connect db mysql://localhost:3306/power success.');
}).catch(function(err) {
	console.error(err);
});

module.exports = {sequelize, User};