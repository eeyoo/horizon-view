var express = require('express');
var router = express.Router();

var db = require('../store/db');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('user', {title: '用户管理'});
});

router.post('/data', function (req, res, next) {

  //var data = null;
  console.log(req.body);
    db.User.findAndCountAll({

    }).then(function(result) {
    	//var count = result.count;
    	//var data = result.rows;
    	//console.log('count: %d, result: %s', count, JSON.stringify(data[0]));
    	res.json(result.rows);
    	//next();
    }).catch(function(err) {
    	console.log(err);
    });

});

router.post('/add', function (req, res, next) {

  db.User.create(req.body).then(function (result) {
    //res.redirect('/user', {result: result})
    //console.log(result);
    res.json({code:1, message: '保存成功'});
  }).catch(function (err) {
    res.json({code:0, message: '保存失败'});
    console.error(err);
  });
});

router.post('/edit', function (req, res, next) {
  db.User.update(req.body).then(function (result) {
    //res.redirect('/user', {result: result})
    res.json({code:1, message: '修改成功'});
  }).catch(function (err) {
    console.error(err);
  });
});

router.post('/delete', function (req, res, next) {
  console.log(req.body);
  db.User.destroy({
    where: { id: req.body.id }
  }).then(function (result) {
    //res.redirect('/user', {result: result})
    res.json({code:1, message: '删除成功'});
  }).catch(function (err) {
    console.error(err);
  });
});

router.post('/save', function (req, res, next) {
  //console.log(req.body);
  //res.json({code:1,message:'保存成功'});
  db.User.create(req.body).then(function (result) {

    //console.log(result);
    res.redirect('/user');
  }).catch(function (err) {
    res.json({code:0, message: '保存失败'});
    console.error(err);
  });
});

module.exports = router;
